import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';

// Must export the config
export const myFirebaseConfig = {
    apiKey: "AIzaSyBQCd888aHNT1hmGjlCzG6vVfX36wUJPB0",
    authDomain: "down-e4f4a.firebaseapp.com",
    databaseURL: "https://down-e4f4a.firebaseio.com",
    storageBucket: "down-e4f4a.appspot.com",
    messagingSenderId: "150139327719"
  };

  const myFirebaseAuthConfig = {
    provider: AuthProviders.Password,
    method: AuthMethods.Password
  }

  @NgModule({
    imports: [
      BrowserModule,
      AngularFireModule.initializeApp(myFirebaseConfig, myFirebaseAuthConfig)
    ],
    declarations: [ AppComponent ],
    bootstrap: [ AppComponent ]
  })
  export class AppModule {}

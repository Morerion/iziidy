import { MordersPage } from './app.po';

describe('morders App', function() {
  let page: MordersPage;

  beforeEach(() => {
    page = new MordersPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

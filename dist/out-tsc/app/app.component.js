var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';
export var AppComponent = (function () {
    function AppComponent(af) {
        var _this = this;
        this.af = af;
        this.tester = false;
        this.af.auth.subscribe(function (auth) {
            if (auth) {
                console.log('auth success');
                _this.user = _this.af.database.object('users/' + auth.uid);
                console.log(_this.user);
                _this.tester = true;
            }
        });
    }
    AppComponent.prototype.logOut = function () {
        this.af.auth.logout();
        this.tester = false;
    };
    AppComponent.prototype.overrideLogin = function () {
        this.af.auth.login({
            provider: AuthProviders.Anonymous,
            method: AuthMethods.Anonymous,
        });
    };
    AppComponent.prototype.login = function (email, password) {
        this.af.auth.login({ email: email, password: password });
    };
    AppComponent.prototype.register = function (email, password) {
        var _this = this;
        this.af.auth.createUser({
            email: email,
            password: password
        }).then(function (data) {
            console.log('data', data);
            if (data.auth.email === email) {
                _this.af.database.object('users/' + data.auth.uid).set({ email: data.auth.email, status: 'user' });
                console.log('В базу отправлено: ' + email);
                console.log('В базу отправлено:' + data.auth.uid);
                console.log('Аккаунт успешно зарегистрирован!');
            }
        });
    };
    AppComponent = __decorate([
        Component({
            selector: 'app-root',
            template: "\n  <nav class=\"navbar navbar-light bg-faded\">\n  <a class=\"navbar-brand\" href=\"#\">Admin panel</a>\n  <ul class=\"nav navbar-nav\">\n    <li class=\"nav-item active\">\n      <a class=\"nav-link\" href=\"#\">Home <span class=\"sr-only\">(current)</span></a>\n    </li>\n    <li class=\"nav-item\">\n      <a class=\"nav-link\" href=\"#\">Link</a>\n    </li>\n    <li class=\"nav-item\">\n      <a class=\"nav-link\" href=\"#\">Link</a>\n    </li>\n    <li class=\"nav-item dropdown\">\n      <a class=\"nav-link dropdown-toggle\" href=\"http://example.com\" id=\"supportedContentDropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">Dropdown</a>\n      <div class=\"dropdown-menu\" aria-labelledby=\"supportedContentDropdown\">\n        <a class=\"dropdown-item\" href=\"#\">Action</a>\n        <a class=\"dropdown-item\" href=\"#\">Another action</a>\n        <a class=\"dropdown-item\" href=\"#\">Something else here</a>\n      </div>\n    </li>\n  </ul>\n  <form class=\"form-inline float-xs-right\">\n    <input class=\"form-control\" type=\"text\" placeholder=\"Search\">\n    <button class=\"btn btn-outline-success\" type=\"submit\">Search</button>\n  </form>\n</nav>\n<br>\n  <div *ngIf='tester == false' class='container'>\n    <input type=\"text\" class=\"form-control\" #email placeholder=\"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 emeil\" />\n    <br>\n    <input type=\"text\" class=\"form-control\" #password placeholder=\"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u043F\u0430\u0440\u043E\u043B\u044C \" />\n    <br>\n    <button class='btn btn-outline-secondary btn-lg btn-block' (click)=\"register(email.value,password.value)\">\u0417\u0430\u0440\u0435\u0433\u0435\u0441\u0442\u0440\u0438\u0440\u043E\u0432\u0430\u0442\u044C\u0441\u044F</button>\n    <hr>\n    <input type=\"text\" class=\"form-control\" #emais placeholder=\"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 emeil\" />\n    <br>\n    <input type=\"text\" class=\"form-control\" #passwords placeholder=\"\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u043F\u0430\u0440\u043E\u043B\u044C \" />\n    <br>\n    <button class='btn btn-outline-secondary btn-lg btn-block' (click)=\"login(emais.value,passwords.value)\">\u0412\u043E\u0439\u0442\u0438</button>\n    <hr>\n    <button class='btn btn-outline-danger' (click)='logOut()'>\u041F\u043E\u043A\u0435\u0434\u0430</button>\n  </div>\n  <div class='container' *ngIf='tester == true'>\n    <h1>EMAIL:<div class=\"alert alert-success\" role=\"alert\">\n  <strong>{{ (user | async)?.email }}</strong>\n</div></h1>\n<h1>STATUS:<div class=\"alert alert-success\" role=\"alert\">\n<strong>{{ (user | async)?.status }}</strong>\n</div></h1>\n     {{ (user | async)?.email }}\n    {{ (user | async)?.status }}\n    <hr>\n    <button class='btn btn-outline-danger' (click)='logOut()'>\u041F\u043E\u043A\u0435\u0434\u0430</button>\n    <a href='https://www.google.ru/webhp?sourceid=chrome-instant&ion=1&espv=2&ie=UTF-8#q=%D0%9A%D1%83%D0%BF%D0%B8%D1%82%D1%8C%20%D0%B1%D1%83%D0%B1%D0%B5%D0%BD%20%D0%B4%D0%BB%D1%8F%20%D0%B8%D0%B7%D0%B3%D0%BD%D0%B0%D0%BD%D0%B8%D1%8F%20%D1%81%D0%BE%D1%82%D0%B0%D0%BD%D1%8B%20%D0%BE%D0%BD%D0%BB%D0%B0%D0%B9%D0%BD%20%D1%82%D1%8E%D0%BC%D0%B5%D0%BD%D1%8C'>\u041D\u0430 \u0432\u0441\u044F\u043A\u0438\u0439 \u0441\u043B\u0443\u0447\u0430\u0439</a>\n  </div>\n  ",
        }), 
        __metadata('design:paramtypes', [AngularFire])
    ], AppComponent);
    return AppComponent;
}());
//# sourceMappingURL=../../../src/app/app.component.js.map